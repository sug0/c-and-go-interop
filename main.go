package main

import (
	"fmt"

	"gitlab.com/sug0/c-and-go-interop/ffi"
)

func main() {
	var buf []byte
	buf = ffi.AppendHello(buf)
	fmt.Println(string(buf))
}
