package ffi

import "unsafe"

// #cgo LDFLAGS: -L../c -lappend_hello
//
// void c_append_hello(void *slice);
import "C"

//export go_byteslice_append
func go_byteslice_append(slice *[]byte, dataPtr *byte, dataLen int) {
	*slice = append(*slice, unsafe.Slice(dataPtr, dataLen)...)
}

func AppendHello(slice []byte) []byte {
	C.c_append_hello(unsafe.Pointer(&slice))
	return slice
}
