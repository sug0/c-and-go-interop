#include <stdint.h>

/* Go `int` type */
typedef int64_t GoInt;

/* layout of a Go slice */
typedef struct {
  void *data;
  GoInt len;
  GoInt cap;
} GoSlice;

/* code imported from Go */
extern void go_byteslice_append(GoSlice *slice, uint8_t *data_ptr,
                                GoInt data_len);

void c_append_hello(GoSlice *slice) {
  go_byteslice_append(slice, (uint8_t *)"hello from c!", 13);
}
